/* Andrey Shevyakov, 2016 */
// CoffeeMate, by Andrey Shevyakov (20055954), SSD Y4, 2016
// Starting code base off the tutorial by David Drohan, 2016, https://ddrohan.gitbooks.io/dynamic-web-dev-labs/content/

var express = require('express');
var nodemailer = require('nodemailer');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var users = require('./routes/users');
var coffee_shops = require('./routes/coffeshops.js');
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(bodyParser.json({limit: '10mb', extended: false }));
app.use(bodyParser.urlencoded({limit: '10mb'})); // Limiting Menu PDF Size
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes);
app.use('/users', users);
app.use('/coffee_shops', coffee_shops);

// Coffee shops data manipulations. "Twin" URLs preferred, as there is no need for extra rerendering.
app.get('/coffee_shops', coffee_shops.findAll);
app.get('/coffee_shops/:id', coffee_shops.findOne);
app.post('/coffee_shops', coffee_shops.addCoffeeShop);
app.post('/coffee_shops/:id', coffee_shops.addShopMenu);
app.get('/coffee_shops/:id/menu', coffee_shops.getShopMenu);
app.put('/coffee_shops/:id', coffee_shops.updateRating);
app.delete('/coffee_shops/:id', coffee_shops.deleteCoffeeShop);
// User registration and login
app.post('/users', users.registerAndLogin);
// Counting number of entries for the about page
app.options('/about', coffee_shops.numberOfEntries);

// Feedback
feedback = function(req, res) {
  var transporter = nodemailer.createTransport('smtps://sumcheapsausage%40gmail.com:VeryEasyPassword@smtp.gmail.com');
  req.body.feedback = req.body.feedback.trim();

  var mail = {
    to: 'sumcheapsausage@gmail.com',
    subject: 'CoffeeMate - Feedback - ' + req.body.author,
    text: req.body.feedback
  };

  if (req.body.feedback)
    transporter.sendMail(mail, function(err, info){
      if(err) res.json("FAIL");
      else res.json("SUCCESS");
    });
  else
    res.json("FAIL");
};

app.post('/contact', feedback);

// 404 error detection
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
