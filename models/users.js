/* Andrey Shevyakov, 2016 */
var mongoose = require('mongoose');

// User data model
var UserSchema = new mongoose.Schema({
    name: { type: String, unique: true, index: true },
    password: String,
    salt: String,
    session_token: String
});


module.exports = mongoose.model('User', UserSchema);
