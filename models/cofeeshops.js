/* Andrey Shevyakov, 2016 */
var mongoose = require('mongoose');

// Coffee shop data model
var CoffeeshopSchema = new mongoose.Schema({
    name: String,
    country: String,
    region: String,
    city: String,
    address: String,
    price_cat: Number,
    lat: Number,
    lon: Number,
    rating: Array,
    menu: String,
    added_by: String
});

module.exports = mongoose.model('Coffeeshop', CoffeeshopSchema);