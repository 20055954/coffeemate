### Application Info ###
* Name: **CoffeeMate**
* Author: Andrey Shevyakov
* Version: 1.0
* URL: bestcoffeemate.herokuapp.com
* Description: **CoffeeMate** is a MEAN-stack app allowing coffee lovers from all over the world to share their favourite coffee places. The project was completed as a part of the "Dynamic Web Development" module offered by Waterford Institute of Technology. 
#
Important: **Vkontakte** and **Facebook** authentication functionality is only available in the hosted version, as the authentication services provided by **Vkontakte** and **Faceebok** have to be restricted to a single specific domain.