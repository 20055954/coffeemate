/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate');

// Main controller
app.controller('main_controller', ['$scope', '$cookies', '$cookieStore', '$route', '$http', '$location',
    function($scope, $cookies, $cookieStore, $route, $http, $location) {

        // Hiding "Register" button if user is logged in
        if ($cookieStore.get("registration") == 1) {
            $cookieStore.remove("registration");
        }

        $scope.$watch(function() {
            return $cookieStore.get("session_user");
        }, function(val) {
            if (val) {
                $scope.username = "Hi, " + val.split(" ")[0] + "!";
                if ($cookieStore.get("vk_pic")) $scope.pic = $cookieStore.get("vk_pic");
                else if ($cookieStore.get("fb_pic")) $scope.pic = $cookieStore.get("fb_pic");
            }
            else {
                $scope.username = null;
                $scope.pic = null;
            }
        });

        $scope.$watch(function() {
            return $cookieStore.get("fb_pic");
        }, function(val) {
            if (val) {
                $scope.pic = val;
            }
        });

        // Welcome message for logged in user and avatar set-up
        if ($cookieStore.get("session_user")) {
            $scope.username = "Hi, " + $cookieStore.get("session_user").split(" ")[0] + "!";

            // Welcome newly registered user
            if ($cookieStore.get("registration") == 2) {
                $scope.msg = 'You are now registered, ' + $cookieStore.get("session_user") + "! Have fun! ";
                $cookieStore.put("registration", 1);
            }
            else {
                // Welcome message depends on current time
                var d = new Date();
                var h = d.getHours();

                if (h <= 11)
                    $scope.msg = 'Welcome back, ' + $cookieStore.get("session_user") + "! Looking for something to give you a morning boost?";
                else if (h >= 17)
                    $scope.msg = 'Welcome back, ' + $cookieStore.get("session_user") + "! Having a date? Or loading up for an all-nighter?";
                else
                    $scope.msg = 'Welcome back, ' + $cookieStore.get("session_user") + "! There's nothing better than a cup of fresh coffee in the afternoon!";
            }
        }
        // Guest welcome message
        else {
            $scope.msg = "Welcome, Guest!";
        }

      // Displaying "Log Out" button if user logged in
       $scope.display_logout = function() {
          if ($cookieStore.get("session_user") != null)
          {
              return { display : 'inline-block' };
          }
          return {display : 'none'};
       };

       // Displaying "Log In" button if user is not logged in
       $scope.display_login = function() {
            if (!$cookieStore.get("session_user"))
            {
                return { display : 'inline-block' };
            }
            return {display : 'none'};
       };

       // Displaying "Register" button if user is not logged in
       $scope.display_register = function() {
            if (!$cookieStore.get("session_user"))
            {
                return { display : 'inline-block' };
            }
            return {display : 'none'};
       };

       // Log out
       $scope.logout = function () {
          $location.path('/');
          try {
          FB.logout(); } catch (err) {}
          VK.Auth.logout();
          $cookieStore.remove('session_user');
          $cookieStore.remove('session_token');
          $cookieStore.remove('fb_id');
          $cookieStore.remove('vk_id');
          $cookieStore.remove('fb_pic');
          $cookieStore.remove('vk_pic');
          $route.reload();
       };

        // Setting font size if saved in browser
       if (localStorage.getItem('font_size') != null) {
            $scope.font_size = localStorage.getItem('font_size');
            $scope.font_size_val = parseInt(localStorage.getItem('font_size'));
            $scope.font_size_app_name = localStorage.getItem('font_size_app_name');
            $scope.font_size_app_name_val = parseInt(localStorage.getItem('font_size_app_name'));
            $scope.height = localStorage.getItem('height');
            $scope.width = localStorage.getItem('width');
            $scope.height_val = parseInt(localStorage.getItem('height'));
            $scope.width_val = parseInt(localStorage.getItem('width'));
       }
       else {
            $scope.font_size = '12pt';
            $scope.font_size_val = 12;
            $scope.font_size_app_name = '17pt';
            $scope.font_size_app_name_val = 17;
            $scope.height = '48px';
            $scope.width = '48px';
            $scope.height_val = 48;
            $scope.width_val = 48;
       }

       // Increasing font size
       $scope.increaseFont = function () {
          if ($scope.font_size_val < 15) {
              $scope.font_size_val += 2;
              $scope.font_size = $scope.font_size_val + 'pt';
              $scope.font_size_app_name_val += 3;
              $scope.font_size_app_name = $scope.font_size_app_name_val + 'pt';
              $scope.width_val += 8;
              $scope.height_val += 8;
              $scope.width = $scope.width_val + 'px';
              $scope.height = $scope.height_val + 'px';

              localStorage.setItem('font_size', $scope.font_size);
              localStorage.setItem('font_size_app_name', $scope.font_size_app_name);
              localStorage.setItem('width', $scope.width);
              localStorage.setItem('height', $scope.height);
          }
       };

       // Decreasing font size
       $scope.decreaseFont = function () {
          if ($scope.font_size_val > 12) {
              $scope.font_size_val -= 2;
              $scope.font_size = $scope.font_size_val + 'pt';
              $scope.font_size_app_name_val -= 3;
              $scope.font_size_app_name = $scope.font_size_app_name_val + 'pt';
              $scope.width_val -= 8;
              $scope.height_val -= 8;
              $scope.width = $scope.width_val + 'px';
              $scope.height = $scope.height_val + 'px';

              localStorage.setItem('font_size', $scope.font_size);
              localStorage.setItem('font_size_app_name', $scope.font_size_app_name);
              localStorage.setItem('width', $scope.width);
              localStorage.setItem('height', $scope.height);
          }
       };

       // User logged in check
       $scope.userLoggedIn = function () {
          if ($cookieStore.get('session_user')) return true;
          return false;
       };

     }
  ]);
