/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate');

// "Cofee Shops" page controller
app.controller('coffee_shops_controller', ['$scope', '$http', '$route', '$cookies', '$cookieStore', '$routeParams', '$location', 'uiGmapGoogleMapApi',
    function($scope, $http, $route, $cookies, $cookieStore, $routeParams, $location, $mapAPI) {

    // Shop deleted notification removal
    if ($cookieStore.get("shop_deleted") == 1) {
            $cookieStore.remove("shop_deleted");
    }

    // Shop added notification removal
    if ($cookieStore.get("shop_added") == 1) {
            $cookieStore.remove("shop_added");
    }

    $scope.res = {};
    $scope.menu_to_upload = null;
    $scope.filter = {country: null, region: null, city: null, price_cat: null};
    $scope.cheap = false;
    $scope.moderate = false;
    $scope.expensive = false;
    $scope.price_categories = [{ name: "Any Price", val: 0 }, { name: "Cheap ($)", val: 1 }, { name: "Moderate ($$)", val: 2 }, { name: "Expensive ($$$)", val: 3}];
    $scope.menuUploadInProgress = false;
    $scope.menuUploaded = false;
    $scope.invalidMenuFile = false;
    $scope.unknownError = false;
    $scope.menuDownloadInProgress = false;
    $scope.menuInGeneration = false;

    // Shop added notification showing time span adjustment
    if ($cookieStore.get("shop_added") == 2) { $cookieStore.put("shop_added", 1); }

    // Shop deleted notification showing time span adjustment
    if ($cookieStore.get("shop_deleted") == 2) {
            $scope.del_name = $cookieStore.get('shop_del_name');
            $scope.del_address = $cookieStore.get('shop_del_address');
            $cookieStore.put("shop_deleted", 1);
    }

    // Retrieving unique entries from array
    $scope.uq = function (uq_arr, prop) {
        for(i = 0; i < $scope.res.length; i++){
            if(uq_arr.indexOf($scope.res[i][prop]) === -1){
                uq_arr.push($scope.res[i][prop]);
            }
        }
    };

    // Shops filtering (country, region, city, price category)
    $scope.filterShops = function(from) {
        $scope.res = JSON.parse(JSON.stringify($scope.pre_filtered_res));

        $scope.unique_regions = ['* (All Regions)'];
        $scope.unique_cities = ['* (All Cities)'];

        switch (from)
        {
            // Country filter
            case 1:
                $scope.filter.region = $scope.unique_regions[0];
                $scope.filter.city = $scope.unique_cities[0];

                if ($scope.filter.price_cat != 0) {
                    $scope.res = $scope.res.filter(function (obj) {
                        return obj.price_cat == $scope.filter.price_cat;
                    });
                }

                if ($scope.filter.country != '* (All Countries)') {
                    $scope.res = $scope.res.filter(function (obj) {
                        return obj.country == $scope.filter.country;
                    });

                    $scope.uq($scope.unique_regions, 'region');
                }
                break;

            // Region filter
            case 2:
                $scope.filter.city = $scope.unique_cities[0];

                if ($scope.filter.price_cat != 0) {
                    $scope.res = $scope.res.filter(function (obj) {
                        return obj.price_cat == $scope.filter.price_cat;
                    });
                }

                if ($scope.filter.country != '* (All Countries)') {
                        $scope.res = $scope.res.filter(function (obj) {
                            return obj.country == $scope.filter.country;
                        });

                        $scope.uq($scope.unique_regions, 'region');

                        if ($scope.filter.region != '* (All Regions)') {
                                $scope.res = $scope.res.filter(function (obj) {
                                    return obj.region == $scope.filter.region;
                            });
                        }

                        $scope.uq($scope.unique_cities, 'city');
                }
                break;

            // City filter
             case 3:
                 if ($scope.filter.price_cat != 0) {
                     $scope.res = $scope.res.filter(function (obj) {
                         return obj.price_cat == $scope.filter.price_cat;
                     });
                 }

                 if ($scope.filter.country != '* (All Countries)') {
                    $scope.res = $scope.res.filter(function (obj) {
                        return obj.country == $scope.filter.country;
                    });

                    $scope.uq($scope.unique_regions, 'region');

                    if ($scope.filter.region != '* (All Regions)') {
                        $scope.res = $scope.res.filter(function (obj) {
                            return obj.region == $scope.filter.region;
                        });

                        $scope.uq($scope.unique_cities, 'city');

                        if ($scope.filter.city != '* (All Cities)')
                        {
                            $scope.res = $scope.res.filter(function (obj) {
                                return obj.city == $scope.filter.city;
                            });
                        }
                    }
                 }

                 break;

            // Price filter
            case 4:
                if ($scope.filter.price_cat != 0) {
                    $scope.res = $scope.res.filter(function (obj) {
                            return obj.price_cat == $scope.filter.price_cat;
                    });
                }

                if ($scope.filter.country != '* (All Countries)') {
                    $scope.res = $scope.res.filter(function (obj) {
                        return obj.country == $scope.filter.country;
                    });

                    $scope.uq($scope.unique_regions, 'region');

                    if ($scope.filter.region != '* (All Regions)') {
                        $scope.res = $scope.res.filter(function (obj) {
                            return obj.region == $scope.filter.region;
                        });

                        $scope.uq($scope.unique_cities, 'city');

                        if ($scope.filter.city != '* (All Cities)')
                        {
                            $scope.res = $scope.res.filter(function (obj) {
                                return obj.city == $scope.filter.city;
                            });
                        }
                    }
                }
                break;
        }

        // If price filter returns zero locations
        if ($scope.res.length == 0)
        {
            if ($scope.filter.city != '* (All Cities)')
                $scope.unique_cities = ['* (All Cities)', $scope.filter.city];
            else
                $scope.unique_cities = ['* (All Cities)'];
            if ($scope.filter.region != '* (All Regions)')
                $scope.unique_regions = ['* (All Regions)', $scope.filter.region];
            else
                $scope.unique_regions = ['* (All Regions)'];
        }
    };

    // All shops retrieval
    $scope.allShops = function() {
            $http.get('/coffee_shops')
                .success(function (data) {
                    $scope.res = data;

                    $scope.uq = function (uq_arr, prop) {
                        for(i = 0; i < $scope.res.length; i++){
                            if(uq_arr.indexOf($scope.res[i][prop]) === -1){
                                uq_arr.push($scope.res[i][prop]);
                            }
                        }
                    };

                    $scope.pre_filtered_res = JSON.parse(JSON.stringify(data));
                    $scope.hdr = "Coffee Shops";

                    $scope.unique_countries = ['* (All Countries)'];
                    $scope.unique_cities = ['* (All Cities)'];
                    $scope.unique_regions = ['* (All Regions)'];

                    $scope.uq($scope.unique_countries, 'country');

                    $scope.filter.price_cat = 0;

                    $scope.filter.country = $scope.unique_countries[0];
                    $scope.filter.region = $scope.unique_regions[0];
                    $scope.filter.city = $scope.unique_cities[0];
                });
    };

    // One shop retrieval
    $scope.oneShop = function() {
             $http.get('/coffee_shops/' + $routeParams.id)
                 .success(function (data) {
                     try {
                     $scope.res = data[0];
                     $scope.hdr = data[0].name;
                     if ($scope.res.message == 'FAIL') $location.path('/coffee_shops');
                     else {
                         $mapAPI.then(function (maps) {
                             maps.visualRefresh = true;

                             $scope.map = {
                                 center: {latitude: $scope.res.lat, longitude: $scope.res.lon}, zoom: 16, options: {
                                     mapTypeControlOptions: {
                                         mapTypeIds: [maps.MapTypeId.ROADMAP, maps.MapTypeId.HYBRID]
                                     }
                                 }
                             };

                             $scope.markers = [{id: 1, showWindow: false}];
                             $scope.h_mpt = '0px';
                             $scope.h_mpb = '30px';
                             $scope.markers[0].latitude = $scope.res.lat;
                             $scope.markers[0].longitude = $scope.res.lon;

                             switch ($scope.res.price_cat) {
                                 case 1:
                                     $scope.res.price_cat = 'Cheap';
                                     $scope.cheap = true;
                                     break;
                                 case 2:
                                     $scope.res.price_cat = 'Moderate';
                                     $scope.moderate = true;
                                     break;
                                 case 3:
                                     $scope.res.price_cat = 'Expensive';
                                     $scope.expensive = true;
                                     break;
                             }

                             $scope.res.full_address = $scope.res.address + " in "
                                 + $scope.res.city + ", " + $scope.res.region + ", "
                                 + $scope.res.country;

                             if ($scope.res.rating.length > 1)
                             {
                                 $scope.res.overall_rating = 0;

                                 for (var i = 0; i < $scope.res.rating.length; i++)
                                 {
                                     $scope.res.overall_rating += $scope.res.rating[i][0];
                                 }

                                 $scope.res.overall_rating = Math.ceil($scope.res.overall_rating / $scope.res.rating.length);

                             }
                             else if ($scope.res.rating.length == 1)
                             {
                                 $scope.res.overall_rating = $scope.res.rating[0][0];
                             }

                         });
                     } } catch (err) { $location.path('/shops'); };
                 });
    };

    // Coffee shop rating retrieval
    $scope.getRating = function(rating) {
        if (!$scope.hover) {
            if (!$scope.res.overall_rating || rating > $scope.res.overall_rating)
                return 'fa fa-star fa-star-off';
            else if ($scope.res.overall_rating || rating <= $scope.res.overall_rating)
                return 'fa fa-star fa-star-on'
        }
        else {
            if ($scope.hover < rating)
                return 'fa fa-star fa-star-off';
            else
                return 'fa fa-star fa-star-on'
        }
    };

    // Mouse over rating star (single shop)
    $scope.starHoverIn = function(no) {
        $scope.hover = no;
    };

    // Mouse leaving rating star (single shop)
    $scope.starHoverOut = function() {
        $scope.hover = null;
    };

    // Displaying price categories in all shops list
    $scope.displayPriceCatRepeat = function(shop, cat) {
        if (shop.price_cat == cat) return { display: 'inline-block'};
        return { display: 'none'};
    };

    // Displaying rating categories in all shops list
    $scope.displayRatingRepeat = function(shop, star) {
        overall_rating = 0;

        for (var i = 0; i < shop.rating.length; i++)
        {
            overall_rating += shop.rating[i][0];
        }

        overall_rating = Math.ceil(overall_rating / shop.rating.length);

        if (star <= overall_rating) return {opacity: '1', cursor: 'default'};
        return {opacity: '0.5', cursor: 'default'};
    };

    // Shop's user rating update
    $scope.updateRating = function(rating) {
        $http.put('/coffee_shops/' + $routeParams.id, {rating: rating}).success(function(msg) {
            if ($scope.res.rating.length > 1)
                $scope.res.overall_rating = Math.ceil(($scope.res.overall_rating + rating)/2);
            else
                $scope.res.overall_rating = rating;
        });
    };

    // Calling file select on "Upload Menu" button click
    $scope.clickUploadMenu = function() {
        angular.element('#upload').trigger('click');
    };

    // Uploading coffee shop menu
    $scope.uploadMenu = function(file) {
        $scope.menuUploaded = false;
        $scope.invalidMenuFile = false;
        $scope.unknownError = false;
        $scope.menuUploadInProgress = true;
        $scope.menuDownloadInProgress = true; // To prevent download attempts whilst the new menu is being uploaded

        $http.post('/coffee_shops/' + $routeParams.id, {file: file}).success(function(msg) {
            $scope.menuUploadInProgress = false;
            $scope.menuDownloadInProgress = false;

            if (msg.message == 'SUCCESS') {
                $scope.menuUploaded = true;
                $scope.res.menu = msg.menu;
            }
            else if (msg.message == 'INVALID TYPE') $scope.invalidMenuFile = true;
            else $location.path('/'); // Tampering attempts, etc.
        }).error(function(msg) {
            $scope.menuUploadInProgress = false;
            $scope.menuDownloadInProgress = false;
            $scope.unknownError = true; }); // E.g. file too big
    };

    // Checking if menu was uploaded and display "Download Menu" button accordingly
    $scope.hasMenu = function() {
            if ($scope.res.menu) return true;
            return false;
    };

    // Downloading menu
    $scope.downloadMenu = function() {

        $scope.menuUploaded = false;
        $scope.menuDownloadInProgress = true;
        $scope.menuUploadInProgress = true; // To prevent menu upload whilst it is retrieved

        $http.get("/coffee_shops/" + $routeParams.id + "/menu").success(function(msg) {
            if (msg == 'FAIL') $location.path('/');
            else {
                $scope.menuDownloadInProgress = false;
                $scope.menuUploadInProgress = false;
                window.open("data:application/pdf;base64," + $scope.res.menu);
            }
        });
    };

    // Checking if the current user added the currently viewed coffee shop
    $scope.owner = function() {
        if ($cookieStore.get('session_user') && $cookieStore.get('session_token'))
        {
            if ($scope.res.added_by) {
                if ($cookieStore.get('session_user') == $scope.res.added_by.split("¬")[0])
                    return true;
            }
        }
        return false;
    };

    // Checking if there are any coffee shops in the database
    $scope.noShops = function() {
        if (!$routeParams.id)
        {
            if ($scope.res.length == 0)
            return true;
        }
        return false;
    };

    // Rendering the page according to the request (single or all shops)
    $scope.req = function() {
        if ($routeParams.id)
        return 'ONE';
        return 'ALL';
    };

    // Shop deleted notification
    $scope.shopDeleted = function() {
        if ($cookieStore.get('shop_deleted'))
        return true;
        else
        return false;
    };

    // Shop added notification
    $scope.shopAdded = function() {
            if ($cookieStore.get('shop_added'))
                return true;
            else
                return false;
    };

    // Deleting shop
    $scope.deleteShop = function() {
        var proceed = confirm("Are You Sure?");
        if (proceed)
        $http.delete('/coffee_shops/' + $routeParams.id).success(function(msg){
            if (msg.message == 'SUCCESS')
            {
                $cookieStore.put('shop_deleted', 2);
                $cookieStore.put('shop_del_name', $scope.res.name);
                $cookieStore.put('shop_del_address', $scope.res.full_address);
                $location.path('/shops');
            }
        });
    };

    // Retrieving single or all shops, depending on the request
    if ($routeParams.id) $scope.oneShop();
    else $scope.allShops();

    // Checking if shops data was received and rendering the page if it was
    $scope.loaded = function() {
        if (!$scope.hdr)
        return false;
        return true;
    };

    // Shop data loading notification
    $scope.loading = function() {
        if (!$scope.hdr)
            return true;
        return false;
    }
  }
  ]);
