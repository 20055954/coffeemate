/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate');

// "Add" page controller
app.controller('add_controller', ['$scope', '$location', '$http', '$cookies', '$cookieStore', 'uiGmapGoogleMapApi',
    function($scope, $location, $http, $cookies, $cookieStore, $mapAPI) {

        // Redirecting to the home page if user not logged in
    if (!$cookieStore.get('session_token'))
        $location.path('/');

    $scope.formData = {};
    $scope.add_called = false;
    $scope.valid_name_regexp = /^[a-zA-Z0-9-_!@%'&\?\$ ]+$/;
    $scope.name_invalid = false;
    $scope.location_invalid = false;
    $scope.shop_exist = false;
    $scope.price_categories = [{ name: "Cheap ($)", val: 1 }, { name: "Moderate ($$)", val: 2 }, { name: "Expensive ($$$)", val: 3}];
    $scope.formData.price_cat = 1;

    // Marking shop's location
    $scope.markers = [
            {
                id: 1,
                showWindow: false,
                options: { draggable: true }
    }];

    // Marker dragged event
    $scope.markers_events = {
        dragend: function (marker, eventName, model, args) {
            // Update map location
            $scope.map.center.latitude = model.latitude;
            $scope.map.center.longitude = model.longitude;

            // Update form address
            $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + model.latitude +
                ',' + model.longitude).success(
                function(msg) {

                    if (msg.results[0].address_components[0].types[0] == 'route' ||
                        msg.results[0].address_components[0].types[0] == 'street_number') {
                        if (msg.results[0].address_components[0].types[0] == 'route')
                            $scope.formData.address = msg.results[0].address_components[0].long_name;
                        else
                            $scope.formData.address = msg.results[0].address_components[0].long_name + " "
                                + msg.results[0].address_components[1].long_name;
                    }

                    updCRS(msg);
                }
            );
        }
    };

    // Default map center (Germany)
    var lat = 52;
    var lon = 13;
    var zoom = 2;

    $scope.markers[0].latitude = lat;
    $scope.markers[0].longitude = lon;

    // Map center if user share their location
    if (navigator.geolocation != null) {
        navigator.geolocation.getCurrentPosition(function (pos) {
            lat = pos.coords.latitude;
            lon = pos.coords.longitude;
            zoom = 12;

            $scope.map = {center: {latitude: lat, longitude: lon}, zoom: zoom};
            $scope.markers[0].latitude = lat;
            $scope.markers[0].longitude = lon;

            $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lon).success(
                function(msg) {
                    updCRS(msg);
                }
            );
        });
    };

    // Initialising Google Map
    $mapAPI.then(function(maps) {
        maps.visualRefresh = true;

        $scope.map = { center: { latitude: lat, longitude: lon }, zoom: zoom, options: {
                streetViewControl: false,
                mapTypeControlOptions: {
                    mapTypeIds: [maps.MapTypeId.ROADMAP, maps.MapTypeId.HYBRID]
                }
        }};

    });

    // Google Map search box typing event
    $scope.searchbox = {
            position: 'top-right',
            events: {
                places_changed: function (searchBox) {
                    res = searchBox.getPlaces()[0];
                    updCRS({results: res});
                    $scope.map = {center: {latitude: res.geometry.location.lat(), longitude: res.geometry.location.lng()}, zoom: 12};
                    $scope.markers[0].latitude = res.geometry.location.lat();
                    $scope.markers[0].longitude = res.geometry.location.lng();
                }
            }
    };

    // Form data address change
    $scope.locationTyped = function(call, from_enter) {

        $scope.location_invalid = false;

        /* Calls:
        1 - country
        2 - region
        3 - city
        4 - address
         */

        if (call != 4) $scope.formData.address = null;

        switch (call) {
            case 1: $scope.formData.region = null;
                    $scope.formData.city = null;
                    $scope.formData.address = null;
                    break;
            case 2: $scope.formData.city = null;
                    $scope.formData.address = null;
                    break;
        }

        var req_string = "http://maps.googleapis.com/maps/api/geocode/json?address=" + $scope.formData.address +
            "&components=administrative_area:" + $scope.formData.region + "|country:" + $scope.formData.country + "|locality:"
            + $scope.formData.city;

        req_string = req_string.replace(/undefined/g, '').replace(/null/g, '');

        // Getting Google Maps location based on the form address data
        $http.get(req_string).success(function(msg) {

            // Valid location
            if (msg.status == 'OK')
            {
                // Updating form location data based on response
                // Useful, when user does not remember the exact name or make a typo
                // Example: enter 'Barreck St' in Waterford, 'Barrack Street' will be returned, the form will be updated accordingly

                if (call == 4 && (msg.results[0].address_components[0].types[0] == 'route' ||
                    msg.results[0].address_components[0].types[0] == 'street_number')) {
                    if (msg.results[0].address_components[0].types[0] == 'route')
                        $scope.formData.address = msg.results[0].address_components[0].long_name;
                    else
                        $scope.formData.address = msg.results[0].address_components[0].long_name + " "
                            + msg.results[0].address_components[1].long_name;
                }

                if (!(call == 4 && (($scope.formData.country != $scope.country_init && $scope.country_init)
                    || ($scope.formData.region != $scope.region_init && $scope.region_init) ||
                    ($scope.formData.city != $scope.city_init && $scope.city_init)))) {

                    if (call == 4 && (msg.results[0].address_components[0].types[0] == 'route' || msg.results[0].address_components[0].types[0] == 'street_number') )
                        updCRS({results: msg.results[0]});
                    else if (call != 4 && msg.results[0].address_components[0].types[0] != 'route' && msg.results[0].address_components[0].types[0] != 'street_number')
                        updCRS({results: msg.results[0]});

                    // Reasonable zoom level
                    if ($scope.formData.address && $scope.formData.city && $scope.formData.region && $scope.formData.country)
                        zoom = 16;
                    else if (!$scope.formData.address && $scope.formData.city && $scope.formData.region && $scope.formData.country)
                        zoom = 12;
                    else if (!$scope.formData.address && !$scope.formData.city && $scope.formData.region && $scope.formData.country)
                        zoom = 9;
                    else if (!$scope.formData.address && !$scope.formData.city && !$scope.formData.region && $scope.formData.country)
                        zoom = 4;

                    // Invalid address entered
                    if ($scope.formData.address != null && msg.results[0].address_components[0].types[0] != 'route'
                        && msg.results[0].address_components[0].types[0] != 'street_number') {
                        $scope.formData.address = null;
                    }
                    else {
                        // Updating Google Map
                        $scope.map = {
                            center: {
                                latitude: msg.results[0].geometry.location.lat,
                                longitude: msg.results[0].geometry.location.lng
                            }, zoom: zoom
                        };
                        $scope.markers[0].latitude = msg.results[0].geometry.location.lat;
                        $scope.markers[0].longitude = msg.results[0].geometry.location.lng;
                    }

                    // Save location data for revert, in case of subsequent invalid requests
                    $scope.country_init = JSON.stringify($scope.formData.country).replace(/"/g, '');
                    $scope.region_init = JSON.stringify($scope.formData.region).replace(/"/g, '');
                    $scope.city_init = JSON.stringify($scope.formData.city).replace(/"/g, '');
                    $scope.address_init = JSON.stringify($scope.formData.address).replace(/"/g, '');
                    if (from_enter)
                    $scope.addCoffeeShop();
                }

            }

            // Invalid location
            else
            {
                // Revert to the previous, valid location
                $scope.formData.country = $scope.country_init;
                $scope.formData.region = $scope.region_init;
                $scope.formData.city = $scope.city_init;
                $scope.formData.address = $scope.address_init;

                if ($scope.formData.country == 'null') $scope.formData.country = null;
                if ($scope.formData.region == 'null') $scope.formData.region = null;
                if ($scope.formData.city == 'null') $scope.formData.city = null;
                if ($scope.formData.address == 'null') $scope.formData.address= null;
            }

            if (!$scope.formData.country || !$scope.formData.region || !$scope.formData.city) $scope.formData.address = null;
        });
    };

    // Adding new shop to the database
    $scope.addCoffeeShop = function() {

        if ($scope.formData.city && $scope.formData.region &&
            $scope.formData.country && $scope.formData.name && $scope.formData.address && $scope.add_called == false)
        {

            $scope.add_called = true;

            var req_string = "http://maps.googleapis.com/maps/api/geocode/json?address=" + $scope.formData.address +
                "&components=administrative_area:" + $scope.formData.region + "|country:" + $scope.formData.country + "|locality:"
                + $scope.formData.city;

            // Rechecking location validity
            $http.get(req_string).success(function(msg) {
                if (msg.status == 'OK') {
                    updCRS(msg);

                    if (msg.results[0].address_components[0].types[0] == 'route' ||
                        msg.results[0].address_components[0].types[0] == 'street_number') {
                        if (msg.results[0].address_components[0].types[0] == 'route')
                            $scope.formData.address = msg.results[0].address_components[0].long_name;
                        else
                            $scope.formData.address = msg.results[0].address_components[0].long_name + " "
                                + msg.results[0].address_components[1].long_name;

                        $http.post('/coffee_shops', $scope.formData).success(function(msg) {
                            if (msg.message == 'FAIL')
                                $location.path('/');
                            else if (msg.message == 'EXIST')
                                $scope.shop_exist = true;
                            else {
                                $cookieStore.put('shop_added', 2);
                                $location.path('/shops/' + msg.data._id);
                            }
                            $scope.add_called = false;
                        });
                    }
                    else
                    {
                        $scope.add_called = false;
                        $scope.shop_exist = false;
                        $scope.location_invalid = true;
                        $scope.formData.country = $scope.country_init;
                        $scope.formData.region = $scope.region_init;
                        $scope.formData.city = $scope.city_init;
                        $scope.formData.address = $scope.address_init;
                        if ($scope.formData.address = 'null') $scope.formData.address = null;
                    }
                } else {
                    $scope.add_called = false;
                    $scope.shop_exist = false;
                    $scope.location_invalid = true;
                }
            });
        }
    };

    // Invalid coffee shop name
    $scope.nameErr = function() {
        if (($scope.formData.name.match($scope.valid_name_regexp) != null && $scope.formData.name.length < 128) ||
            !$scope.formData.name)
            $scope.name_invalid = false;
        else
            $scope.name_invalid = true;
    };

    // "Enter" key press handling when filling in location data in form
    $scope.formEnter = function(call, $event) { if ($event.keyCode == 13) $scope.locationTyped(call, true); };

    // Blocking submit button in case new entry is being created or form data is invalid
    $scope.invalidFormOrInProgress = function() {
        if ($scope.formData.city && $scope.formData.region &&
            $scope.formData.country && $scope.formData.name && $scope.formData.address
            && $scope.formData.name.match($scope.valid_name_regexp) != null && !$scope.add_called)
            return false;
        return true;
    };

    // Update form location data based on Google Maps response
    var updCRS = function(msg) {
        if (msg.results.length == null)
            msg.results = [msg.results];

        for (var i = 0; i < msg.results.length; i++)
        {
            for (var z = 0; z < msg.results[i].address_components.length; z++) {
                if (msg.results[i].address_components[z].types[0] == 'locality')
                    $scope.formData.city  = msg.results[i].address_components[z].long_name;
                else if (msg.results[i].address_components[z].types[0] == 'administrative_area_level_1')
                    $scope.formData.region = msg.results[i].address_components[z].long_name;
                else if (msg.results[i].address_components[z].types[0] == 'country')
                    $scope.formData.country = msg.results[i].address_components[z].long_name;
            }
        }
    };

  }
  ]);
