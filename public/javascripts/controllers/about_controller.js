/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate');

// "About" page controller
app.controller('about_controller', ['$scope', '$cookies', '$cookieStore', '$http', function($scope, $cookies, $cookieStore, $http) {
    $scope.welcoming_msg = 'Welcome to CoffeeMate';

    if ($cookieStore.get('session_user'))
    $scope.welcoming_msg = $scope.welcoming_msg + ', ' + $cookieStore.get('session_user');

    // Retrieving the number of shops' entries in the database
    $http({
        method: "OPTIONS",
        url: "/about",
    }).success(function(msg) {
        if (msg.num != 0) // Also returned in case of error
        {
            if (msg.num == 1)
                $scope.loc_num = msg.num + " location in the database and counting!";
            else
                $scope.loc_num = msg.num + " locations in the database and counting!";
        }
    });
}
]);