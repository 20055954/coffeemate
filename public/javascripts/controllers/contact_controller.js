/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate');

// "Contact" page controller
app.controller('contact_controller', ['$scope', '$http', function($scope, $http) {
    $scope.formData = {};
    $scope.contact_in_progress = false;
    $scope.error_notification = false;
    $scope.success_notification = false;

    // Sending feedback form
    $scope.send = function() {

        $scope.contact_in_progress = true;
        $scope.error_notification = false;
        $scope.success_notification = false;

        var data = {
            'author' : $scope.formData.email,
            'feedback' : $scope.formData.feedback
        };

        // Calling PHP mailer
        $http({
            method: 'POST',
            url: '/contact',
            data: data
        }).
        success(function(msg) {
            if (msg == 'SUCCESS') {
                $scope.success_notification = true;
                $scope.formData.email = null;
                $scope.formData.feedback = null;
            }
            else
                $scope.error_notification = true;
            $scope.contact_in_progress = false;
        }).
        error(function(msg) {
            $scope.contact_in_progress = false;
            $scope.error_notification = true;
        });
        return false;
    };

    // Feedback form submission handling
    $scope.sendFeedback = function () {
        $scope.send();
    };

    // Feedback form validity check
    $scope.invalidForm = function () {
        try {
            if ($scope.contact_in_progress == true || $scope.formData.feedback.length == 0 ||
                    $scope.formData.email.split("@")[1].split(".").length != 2 ||
                    $scope.formData.email.split("@")[1].split(".")[1].length < 2)
                    return true;
            return false;
        } catch (err) { return true; }
    };

}
]);