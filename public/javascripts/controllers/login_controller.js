/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate');

// "Login" page controller
app.controller('login_controller', ['$scope', '$location', '$cookies', '$cookieStore', '$http', '$route',
    function($scope, $location, $cookies, $cookieStore, $http, $route) {

    $scope.formData = {};
    $scope.creds_invalid = false;

    // Redirecting to the home page if user already logged in
    if ($cookieStore.get('session_token'))
    $location.path('/');

    // Login (website registration)
    $scope.login = function() {
        $scope.formData.login = 1;
        $http.post('/users', $scope.formData).success(function(msg){

            if (msg['msg'] == 'Auth Fail')
                $scope.creds_invalid = true;
            else
            {
                $cookieStore.put("session_user", $scope.formData.name);
                $cookieStore.put("session_token", msg['session_token']);
                $location.path("/");
            }

        });
    };

    // Login (FB)
    $scope.login_fb = function() {
        FB.login(function() {
            FB.getLoginStatus(function(res) {
                $http.get('https://graph.facebook.com/' + res.authResponse.userID +
                    '?fields=name&access_token=' + res.authResponse.accessToken).success(function(msg) {
                    $http.post('/users', {fb_login: 1, login: 1, fb_id: res.authResponse.userID, fb_name: msg['name'],
                        session_token: res.authResponse.accessToken});
                    $cookieStore.put("fb_id",  res.authResponse.userID);
                });
            });
        })
    };

    // Login (VK)
    $scope.login_vk = function() {
        VK.Auth.login(function() {
            var authInfo = function (response) {
                if (response.session) {
                    VK.api('users.get',{uids: response.session.mid, access_token: response.session.sid, fields: "photo_50"},
                        function(msg) {
                            if ($cookieStore.get("session_user") == null && $cookieStore.get("session_token") == null) {
                                vk_name = msg.response[0].first_name + " " + msg.response[0].last_name;
                                $cookieStore.put("vk_id", response.session.mid);
                                $cookieStore.put("vk_pic", msg.response[0].photo_50);
                                $cookieStore.put("session_user", vk_name);
                                $cookieStore.put("session_token", response.session.sid);
                                $http.post('/users', {vk_login: 1, login: 1, vk_id: response.session.mid, vk_name: vk_name,
                                    session_token: response.session.sid});
                                $route.reload();
                            }
                        });
                }
            };

            VK.Auth.getLoginStatus(authInfo);
            $location.path('/');
        });
    };

}
]);
