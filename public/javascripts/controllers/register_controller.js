/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate');

// "Register" page controller
app.controller('register_controller', ['$scope', '$http', '$location', '$cookies', '$cookieStore', function($scope, $http, $location, $cookies, $cookieStore) {

    // Redirecting to the home page if user logged in
    if ($cookieStore.get("session_user"))
    {
            if ($location.path() == '/register')
            $location.path('/');
    }

    $scope.formData = {};
    $scope.password_invalid = false;
    $scope.password_too_long = false;
    $scope.password_too_short = false;
    $scope.name_invalid = false;
    $scope.name_too_long = false;
    $scope.name_too_short = false;
    $scope.name_taken = false;
    $scope.registration_in_progress = false;
    $scope.valid_name_regexp = /^[a-zA-Z0-9-_]+$/;
    $scope.valid_password_regexp = /^[a-zA-Z0-9-_!@#\$\+%\^&*\(\)\[\]\{\}|\\/\?]+$/;

    // Invalid name error notification
    $scope.nameErr = function () {

        $scope.name_taken = false;
        $scope.name_invalid = false;
        $scope.name_too_long = false;
        $scope.name_too_short = false;

        if ($scope.formData.name.length < 2)
        $scope.name_too_short = true;
        else if ($scope.formData.name.length > 64)
        $scope.name_too_long = true;

        if ($scope.formData.name.match($scope.valid_name_regexp) == null && $scope.formData.name.length > 1
            ||
            ($scope.formData.name.match(/[a-z]/i) == null && $scope.formData.name.length > 1))
        $scope.name_invalid = true;

    };

    // Invalid password error notification
    $scope.passwordErr = function () {

        $scope.name_taken = false;
        $scope.password_invalid = false;
        $scope.password_too_long = false;
        $scope.password_too_short = false;

        if ($scope.formData.password.length < 6)
        $scope.password_too_short = true;
        else if ($scope.formData.password.length > 64)
        $scope.password_too_long = true;

        if ($scope.formData.password.match($scope.valid_password_regexp) == null && $scope.formData.password.length > 5)
        $scope.password_invalid = true;

    };

    // Registration form validity check
    $scope.invalidForm = function () {
        try {
            if ($scope.registration_in_progress == true ||
                $scope.formData.password.length < 6 || $scope.formData.password.length > 64
                || $scope.formData.password != $scope.formData.passwordConfirmation || $scope.formData.name.length < 2
                || $scope.formData.name.length > 64 || $scope.formData.name.match($scope.valid_name_regexp) == null ||
                $scope.formData.password.match($scope.valid_password_regexp) == null)
                return true;
            return false;
        } catch (err) { return true; }

    };

    // Registration
    $scope.register = function () {
        $scope.registration_in_progress = true;
        $scope.name_taken = false;

        // Creating new user entry in the database
        $http.post('/users', $scope.formData)
        .success(function(msg){
            $scope.registration_in_progress = false;
            $scope.formData.password = null;
            $scope.formData.passwordConfirmation = null;

            if (msg['msg'] == 'Nickname Already Taken')
            {
                $scope.formData.name = null;
                $scope.name_taken = true;
            }
            else
            {
                $cookieStore.put("session_user", msg['data'].name);
                $cookieStore.put("session_token", msg['data'].session_token);
                $cookieStore.put("registration", 2); // has to be 2 in order to avoid the callback issue
                $location.path("/");
            }
        });
    };

}
]);
