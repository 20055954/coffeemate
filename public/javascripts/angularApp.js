/* Andrey Shevyakov, 2016 */
var app = angular.module('CoffeeMate', ['ngRoute', 'ngCookies', 'nemLogging', 'uiGmapgoogle-maps', '720kb.tooltips']);

// Initialising Google Map provider
app.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
    GoogleMapApi.configure({
        libraries: 'weather,geometry,visualization, places'
    });
}]);

// Routes configuration
app.config(function($routeProvider,$locationProvider) {

        $routeProvider

            // Home page
            .when('/', {
                templateUrl : '/pages/home.ejs',
                controller  : 'main_controller'
            })

             // Add shop page
            .when('/add', {
                templateUrl : '/pages/add.ejs',
                controller  : 'add_controller'
            })

             // List all shops page
            .when('/shops', {
                templateUrl : '/pages/coffee_shops.ejs',
                controller  : 'coffee_shops_controller'
            })

            // View single shop page
            .when('/shops/:id', {
                templateUrl : '/pages/coffee_shops.ejs',
                controller  : 'coffee_shops_controller'
            })

            // Contact page
            .when('/contact', {
                templateUrl : '/pages/contact.ejs',
                controller  : 'contact_controller'
            })

            // About page
            .when('/about', {
                templateUrl : '/pages/about.ejs',
                controller  : 'about_controller'
            })

            // Registration page
            .when('/register', {
                templateUrl: '/pages/register.ejs',
                controller : 'register_controller'
            })

            // Login page
            .when('/login', {
                templateUrl: '/pages/login.ejs',
                controller : 'login_controller'
            })

            // Redirecting to main page if requested route is invalid
            .otherwise({
            redirectTo: '/'
            });

            // Removing # from URLs
            $locationProvider.html5Mode({
                    enabled: true,
                    requireBase: false
            })

    });

app.run(function($window, $location, $http, $cookies, $cookieStore, $route) {

    // Initialising FB API
    $window.fbAsyncInit = function() {
        FB.init({
            appId: '1027807323932328',
            status: true,
            cookie: true,
            xfbml: true,
            version: 'v2.4'
        });

        // FB login handling
        FB.Event.subscribe('auth.authResponseChange', function(res) {
            if (res.status === 'connected') {
                if (!$cookieStore.get("session_user")) {
                    $http.get("https://graph.facebook.com/" + res.authResponse.userID +
                        "?fields=name&access_token=" + res.authResponse.accessToken).success(function (msg) {
                        $cookieStore.put("session_user", msg['name']);
                        $cookieStore.put("session_token", res.authResponse.accessToken);
                        if ($location.path() == '/login') {
                            FB.api(
                                "/" + res.authResponse.userID + "/picture",
                                function (response) {
                                    if (response && !response.error) {
                                        $cookieStore.put("fb_pic",  response.data.url);
                                        $location.path('/');
                                        $route.reload();
                                    }
                                }
                            );
                        }
                        $route.reload();
                    });
                }
            }
        });
    };

    // By Bruno Scopelliti, 2013, http://blog.brunoscopelliti.com/facebook-authentication-in-your-angularjs-web-app/
    (function(d){
        // injecting Facebook javascript SDK into web page

        var js,
            id = 'facebook-jssdk',
            ref = d.getElementsByTagName('script')[0];

        if (d.getElementById(id)) {
            return;
        }

        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";

        ref.parentNode.insertBefore(js, ref);

    }(document));

});

// Converting coffee shop PDF menu file to base64 for storage in the database
// Modified Code By Joao Leal, 2014, http://stackoverflow.com/questions/25833323/angularjs-call-metod-from-controller-in-directive
app.directive("fileread", [function () {
    return {
        scope: {
            fileread: '='
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();

                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;

                        if (scope.fileread.split(";")[0] != 'data:application/pdf')
                            scope.$parent.invalidMenuFile = 'inline-block';
                        else
                            scope.$parent.uploadMenu(scope.fileread);
                    });
                };

                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);
