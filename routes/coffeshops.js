/* Andrey Shevyakov, 2016 */
var CoffeeShop = require('../models/cofeeshops');
var User = require('../models/users');
var express = require('express');
var router = express.Router();
var http = require('http');
var mongoose = require('mongoose');
var mongodbUri = 'mongodb://admin:QSQSQS@ds021969.mlab.com:21969/coffee_mate'; // Database credentials

mongoose.connect(mongodbUri); // Database connection

// All shops retrieval
router.findAll = function(req, res) {
    CoffeeShop.find(function(err, shops) {
        if (err)
            res.json({ message: 'FAIL'});
        else {
            for (var i = 0; i < shops.length; i++)
            {
                // Avoiding sending unrequested menu data and saving traffic and workload
                if (shops[i].menu) shops[i].menu = 'menu';
            }
            res.json(shops);
        }
    });
};

// One shop retrieval
router.findOne = function(req, res) {
    CoffeeShop.find({ "_id" : req.params.id },function(err, shop) {
        if (err)
            res.json({ message: 'FAIL'});
        else
        {
            if (shop.menu) shop.menu = 'menu' // Avoiding sending unrequested menu data and saving traffic and workload
            res.json(shop);
        }
    });
};

// Add new shop
router.addCoffeeShop = function(req, res) {

    // Location validity check
    var req_string = "http://maps.googleapis.com/maps/api/geocode/json?address=" + req.body.address +
        "&components=administrative_area:" + req.body.region + "|country:" + req.body.country + "|locality:"
        + req.body.city;

    // Adjust username to find the proper user entry in the database
    // Website user entry format: username
    // VK user entry format: username¬VK¬2sa353A (after second ¬, VK session token)
    // FB user entry format: username¬FB¬456F23zA (after second ¬, VK session token)
    if (req.cookies.vk_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬VK¬" + req.cookies.vk_id.slice(1, req.cookies.vk_id.length-1);
    else if (req.cookies.fb_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬FB¬" + req.cookies.fb_id.slice(1, req.cookies.fb_id.length-1);
    else
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1);

    req.body.name = (req.body.name.charAt(0).toUpperCase() + req.body.name.slice(1)).trim();

    // Invalid data check
    if (req.body.price_cat < 1 || req.body.price_cat > 3
    || req.body.name.match(/^[a-zA-Z0-9-_!@%'&\?\$ ]+$/) == null || req.body.rating) res.json({message: 'FAIL'});
    else
    User.find({ "name" : req.cookies.session_user },function(err, user) {
        if (!err)
        {
            if (user.length == 1)
            {
                // Check the session's validity
                if (user[0].session_token === req.cookies.session_token.replace(/"/g, ''))
                {
                    // Retrieving the coffee shop's Google Maps location again, in order to prevent inconsistent form and Google Map locations
                    http.get(req_string, function(msg) {

                        var body_chunks = [];

                        // Parsing response
                        // Parsing code by maerics, 2012, http://stackoverflow.com/questions/9577611/http-get-request-in-node-js-express
                        msg.on('data', function(chunk) {
                            body_chunks.push(chunk);
                        }).on('end', function() {
                            var body = Buffer.concat(body_chunks);
                            msg = JSON.parse(body);

                            if (msg.status == 'OK') {
                                // Invalid location - no address specified
                                if (msg.results[0].address_components[0].types[0] == 'route' ||
                                    msg.results[0].address_components[0].types[0] == 'street_number') {

                                    CoffeeShop.find(function(err, shops) {
                                        if (err)
                                            res.json({message: 'FAIL'});
                                        else
                                        {
                                            var unique = true;

                                            for (var i = 0; i < shops.length; i++)
                                            {
                                                if (shops[i].country == req.body.country &&
                                                shops[i].region == req.body.region &&
                                                shops[i].city == req.body.city &&
                                                shops[i].address == req.body.address &&
                                                shops[i].name == req.body.name)
                                                {
                                                    unique = false;
                                                    break;
                                                }
                                            }

                                            if (unique == false) res.json({message: 'EXIST'});
                                            else
                                            {

                                                var shop = new CoffeeShop();

                                                shop.name = req.body.name;
                                                shop.country = req.body.country;
                                                shop.region = req.body.region;
                                                shop.city = req.body.city;
                                                shop.address = req.body.address;
                                                shop.price_cat = req.body.price_cat;
                                                shop.lat = msg.results[0].geometry.location.lat;
                                                shop.lon = msg.results[0].geometry.location.lng;
                                                shop.added_by = req.cookies.session_user;

                                                shop.save(function(err) {
                                                    if (err) res.json({message: 'FAIL'});
                                                    res.json({ message: 'SUCCESS', data: shop});
                                                });
                                            }
                                        }
                                    });

                                } else res.json({message: 'FAIL'});
                            }
                        });

                    });
                } else res.json({message: 'FAIL'});
            } else res.json({message: 'FAIL'});
        }
        else res.json({message: 'FAIL'});
    });
};

// Deleting shop
router.deleteCoffeeShop = function(req, res) {
    if (req.cookies.vk_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬VK¬" + req.cookies.vk_id.slice(1, req.cookies.vk_id.length-1);
    else if (req.cookies.fb_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬FB¬" + req.cookies.fb_id.slice(1, req.cookies.fb_id.length-1);
    else
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1);

    User.find({ "name" : req.cookies.session_user },function(err, user) {
        if (!err) {
            if (user.length == 1) {
                if (user[0].session_token === req.cookies.session_token.replace(/"/g, '')) {
                    CoffeeShop.findById(req.params.id, function(err, shop) {
                        if (err)
                            res.json({message: 'FAIL'});
                        else
                        {
                            if (shop.added_by == req.cookies.session_user) {
                                shop.remove();
                                res.json({message: 'SUCCESS'});
                            }
                            else res.json({message: 'FAIL'})
                        }
                    });
                } else res.json({message: 'FAIL'});
            } else res.json({message: 'FAIL'});
        } else res.json({message: 'FAIL'});
    });
};

// Updating shop rating
router.updateRating = function(req, res) {
    if (req.cookies.vk_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬VK¬" + req.cookies.vk_id.slice(1, req.cookies.vk_id.length-1);
    else if (req.cookies.fb_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬FB¬" + req.cookies.fb_id.slice(1, req.cookies.fb_id.length-1);
    else
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1);

    User.find({ "name" : req.cookies.session_user },function(err, user) {
        if (!err) {
            if (user.length == 1) {
                if (user[0].session_token === req.cookies.session_token.replace(/"/g, '')) {
                    CoffeeShop.findById(req.params.id, function(err,shop) {
                        if (err) res.json({message: 'FAIL'});
                        else {
                            if (shop.rating.length == 0)
                                shop.rating = [[req.body.rating, req.cookies.session_user]];
                            else {
                                var exist = false;

                                for (var i = 0; i < shop.rating.length; i++)
                                {
                                    if (shop.rating[i][1] == req.cookies.session_user)
                                    {
                                        shop.rating[i][0] = req.body.rating;
                                        exist = true;
                                        break;
                                    }
                                }

                                if (!exist) shop.rating.push([req.body.rating, req.cookies.session_user]);

                                shop.markModified('rating');
                            }

                            shop.save(function (err) {
                                if (err)
                                    res.json({message: 'FAIL'});
                                else {
                                    res.json({message: 'SUCCESS'});
                                }
                            });
                        }
                    });
                } else res.json({message: 'FAIL'});
            } else res.json({message: 'FAIL'});
        } else res.json({message: 'FAIL'});
    });
};

// Uploading shop menu
router.addShopMenu = function(req, res) {
    if (req.cookies.vk_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬VK¬" + req.cookies.vk_id.slice(1, req.cookies.vk_id.length-1);
    else if (req.cookies.fb_id)
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1)
            + "¬FB¬" + req.cookies.fb_id.slice(1, req.cookies.fb_id.length-1);
    else
        req.cookies.session_user = req.cookies.session_user.slice(1, req.cookies.session_user.length-1);

    User.find({ "name" : req.cookies.session_user },function(err, user) {
        if (!err) {
            if (user.length == 1) {
                if (user[0].session_token === req.cookies.session_token.replace(/"/g, '')) {
                    CoffeeShop.findById(req.params.id, function(err, shop) {
                        if (err)
                            res.json({message: 'FAIL'});
                        else
                        {
                            if (shop.added_by == req.cookies.session_user) {
                                if (req.body.file.split(";base64,")[0] != 'data:application/pdf')
                                    res.json({message: 'INVALID TYPE'});
                                else
                                {
                                    shop.menu = req.body.file.split(";base64,")[1];
                                    shop.save(function (err) {
                                        if (err) res.json({message: 'FAIL'});
                                        else res.json({message: 'SUCCESS', menu: shop.menu});
                                    });
                                }
                            }
                            else res.json({message: 'FAIL'})
                        }
                    });
                } else res.json({message: 'FAIL'});
            } else res.json({message: 'FAIL'});
        } else res.json({message: 'FAIL'});
    });
};

// Shop menu retrieval
router.getShopMenu = function(req, res) {
    CoffeeShop.find({ "_id" : req.params.id },function(err, shop) {
        if (err)
            res.json('FAIL');
        else
            res.json(shop.menu);
    });
};

// Number count of shop entries in the database
router.numberOfEntries = function(req, res) {
    CoffeeShop.count(function (err, count) {
        if (err) res.json({num: 0});
        else res.json({num: count});
    });
};

module.exports = router;
