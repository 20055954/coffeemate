/* Andrey Shevyakov, 2016 */
var User = require('../models/users');
var express = require('express');
var CryptoJS = require("crypto-js");
var router = express.Router();

// User registration and login
router.registerAndLogin = function(req, res) {

  // Registration
  if (req.body.login == null) {
    req.body.name = req.body.name.trim();

    var user = new User();
    user.name = req.body.name;
    if (req.body.password.length < 6 || req.body.password.length > 64)
      user.password = null;
    else {
      user.salt = randGeneration(36, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()');
      user.password = CryptoJS.PBKDF2(req.body.password, user.salt, {keySize: 128 / 32});
    }

    user.session_token = randGeneration(36, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()');

    valid_name_regexp = /^[a-zA-Z0-9-_]+$/;
    valid_password_regexp = /^[a-zA-Z0-9-_!@#\$\+%\^&*\(\)\[\]\{\}|\\/\?]+$/;

    if (user.name.length < 2 || user.name.length > 64 || !user.password ||
        user.name.match(valid_name_regexp) == null || user.password.match(valid_password_regexp) == null)
      res.send({msg: 'FAIL'});

    else {
      user.save(function (err) {

        if (err)
          res.send({msg: "Nickname Already Taken"});
        else
          res.send({msg: 'User Registered!', data: user});
      });
    }
  }

  // Login
  else
  {
    // Create new user from VK or FB data, if does not exist.
    if (req.body.vk_login || req.body.fb_login)
    var user = new User();

    // VK login
    if (req.body.vk_login != null) {
      user.name = req.body.vk_name + "¬VK¬" + req.body.vk_id;
      user.session_token = req.body.session_token;
      user.save(function(err){
        if (err)
        {
          User.find({"name" : user.name}, function(err,user_f) {
            if (!err)
            {
              user_f[0].session_token = user.session_token;
              user_f[0].save();
            }
          })
        }
      });
    }

    // FB login
    else if (req.body.fb_login != null) {
      user.name = req.body.fb_name + "¬FB¬" + req.body.fb_id;
      user.session_token = req.body.session_token;
      user.save(function(err){
        if (err)
        {
          User.find({"name" : user.name}, function(err,user_f) {
            if (!err)
            {
              user_f[0].session_token = user.session_token;
              user_f[0].save();
            }
          })
        }
      });
    }

    // Website login
    else {
      var user = new User();
      user.name = req.body.name;

      User.find({ "name" : req.body.name },function(err, user) {
        if (err)
          res.json({ msg: 'Auth Fail' } );
        else {
          user = user[0];
          if (user != null && CryptoJS.PBKDF2(req.body.password, user.salt, {keySize: 128 / 32}) == user.password) {
            token = randGeneration(36, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()');

            user.session_token = token;
            user.save();

            res.json({msg: 'Auth Success', session_token: token});
          }
          else
            res.json({ msg: 'Auth Fail' } );
        }
      });
    }
  }
};

// Generating session tokens and salts
// By Nimphious, 2012, http://stackoverflow.com/questions/10726909/random-alpha-numeric-string-in-javascript
var randGeneration = function (length, chars) {
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
};

module.exports = router;

